package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
	
	@FXML
	private Label resultado;
	private long n1 = 0, n2 = 0;
	private String operacao = "";
	private boolean start = true;
	private Model model = new Model();
	
	@FXML
	public void processNumbers (ActionEvent event) {
		
		if(start) {
			resultado.setText("");
			start = false;
		}
		
		String value = ((Button)event.getSource()).getText();
		resultado.setText(resultado.getText() + value);
	}
	
	@FXML
	public void processOperators (ActionEvent event) {
		
		String value = ((Button)event.getSource()).getText();
		
		if(!value.equals("=")) {
			if(!operacao.isEmpty())
				return;
			
			operacao = value;
			n1 = Long.parseLong(resultado.getText());
			resultado.setText("");
			
		} else {
			
			if(operacao.isEmpty())
				return;
			
			n2 = Long.parseLong(resultado.getText());
			float output = model.calculate(n1, n2, operacao);
			resultado.setText(String.valueOf(output));
			operacao = "";
			start = true;
		}
}
}